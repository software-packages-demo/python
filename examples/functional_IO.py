# from dataclasses import dataclass... new in version 3.7

class lazy:
    def __init__(self, f):
        self.__f = f
    def __call__(self): # args*):
        return self.__f() #args*)

# ABC

class IO:
    def __init__(self, v):
        self.__v = v
    def __repr__(self):
        return 'IO('+repr(self.__v)+')'
    def lift(self, f):
        result = f(self.__v)
        # assert(IO = result.type) or with ABC
        return result


print('main')

print(lazy(lambda: print('Hello')))
lazy(lambda: print('Hello just lazy'))()


print(IO(print('Hello!')))

print(IO(input('Tell something!:>>> ')))

print(IO(input("What's your name?>>> "))
    .lift(lambda v: IO(print('Hello', v))))

print('END>>> ')
